import 'package:redux_practice_new/model/app_state.dart';

AppState appStateReducer(AppState state, action) {
  return AppState();
}

CounterState counterStateReducer(AppState state, action) {
  return CounterState();
}

GalleryState galleryStateReducer(AppState state, action) {
  return GalleryState();
}